﻿using MaterialSkin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatMessenger
{
    public partial class Login : Form
    {
        private String username;
        private String password;
        public Login()
        {
            /* Initialise la fenetre avec le design Material */
            InitializeComponent();
            MaterialSkinManager manager = MaterialSkinManager.Instance;
   
       
            
            manager.Theme = MaterialSkinManager.Themes.LIGHT;
            manager.ColorScheme = new ColorScheme(Primary.Blue400, Primary.Blue500, Primary.Blue500, Accent.LightBlue200, TextShade.WHITE);
            
        }

        private void UserText_TextChanged(object sender, EventArgs e)
        {
            username = UserText.Text;

        }

        private void PasswordText_TextChanged(object sender, EventArgs e)
        {
            password = PasswordText.Text;
        }

        private void LaunchButton_Click(object sender, EventArgs e)
        {
            DB db = new DB();
            if (db.VerificationCo(username, password))
            {
                Chat ch = new Chat();
                ch.Show();
                this.Hide();
             }else
            {
                MessageBox.Show("Mot de passe et login incorrect");
            }
        }
    }
}
