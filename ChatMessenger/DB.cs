﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatMessenger
{
    class DB
    {
        private MySqlConnection db;
        MySqlDataAdapter adtr;
        MySqlDataReader dr;
        private MySqlCommand cmd = new MySqlCommand();
        private String _password ="test";
        private String _username="root";

        public DB()
            {
                db = new MySqlConnection($"Server=localhost;Database=rtc;Uid={_username};Pwd={_password};");
                
            }


        public DB(String password, String username)
        {
            db = new MySqlConnection($"Server=localhost;Database=rtc;Uid={username};Pwd={password};");
        }

        public void addMessage(String fullname,String msg)
        {
            cmd = new MySqlCommand("Insert INTO message(time,fullname,msg) VALUES(NOW(),'"+fullname+"','"+msg+"')",db);
            cmd.CommandTimeout = 40;

            try
            {
                db.Open();
                MySqlDataReader myReader = cmd.ExecuteReader();
                MessageBox.Show("Utilisateur connecté");
                db.Close();
            }catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }

        }

        public bool VerificationCo(String user, String pass)
        {
            cmd =new MySqlCommand("SELECT * FROM user WHERE username='"+user+"' AND password='"+pass+"'",db);
            cmd.CommandTimeout = 30;

            MySqlDataReader reader;

            try
            {

                db.Open();
                reader = cmd.ExecuteReader();
              
                if (reader.HasRows)
                {
                    db.Close();
                    return true;
                  
                }
                else
                {
                    db.Close();
                    return false;
                 
                }
            }catch(Exception e)
            {
                MessageBox.Show(e.Message);
                db.Close();
                return false;
            }


        }

        public String username
        {
            get => _username;
            set => _username = value;
        }

        public String password
        {
            get => password;
            set => _password = value;
        }

    }
}
