﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace ChatMessenger
{
    public partial class Chat : MaterialForm
    {

        private TcpClient client;
        public StreamReader STR;
        public StreamWriter STW;
        public string recieve;
        public String TextToSend;

        public Chat()
        {
            /* Initialise la fenetre avec le design Material */
            InitializeComponent();
            MaterialSkinManager manager = MaterialSkinManager.Instance;
            manager.AddFormToManage(this);
            manager.Theme = MaterialSkinManager.Themes.LIGHT;
            manager.ColorScheme = new ColorScheme(Primary.Blue400, Primary.Blue500, Primary.Blue500, Accent.LightBlue200, TextShade.WHITE);

            /* Permet de récupérer et d'afficher l'ip de l'host*/
            IPAddress[] localIP = Dns.GetHostAddresses(Dns.GetHostName());

            foreach (IPAddress address in localIP)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                {
                    ServerIpTextBox.Text = address.ToString();
                }
            }

        }
        /*Verifie que le string entrée peut etre parsé*/
        private bool tryParseInt(String str)
        {
            try
            {
                int.Parse(str);
                return true;
            }catch(Exception)
            {
                return false;
            }
        }

        /* Boutton démarrer, permettant de démarrer le serveur */
        private void StartButton_Click(object sender, EventArgs e)
        {
            String firstPort = ServerPortTextBox.Text;
            if (tryParseInt(firstPort))
            {
                TcpListener listener = new TcpListener(IPAddress.Any, int.Parse(firstPort));
                listener.Start();
                client = listener.AcceptTcpClient();
                STR = new StreamReader(client.GetStream());
                STW = new StreamWriter(client.GetStream());
                STW.AutoFlush = true;

                backgroundWorker1.RunWorkerAsync();
                backgroundWorker2.WorkerSupportsCancellation = true;
            }
        }


        /*Boutton connexion, permettant de connecter le client au serveur*/
        private void ConnectButton_Click(object sender, EventArgs e)
        {
            String secondPort = ClientPortTextBox.Text;
            if (tryParseInt(secondPort))
            {
                client = new TcpClient();
                IPEndPoint IpEnd = new IPEndPoint(IPAddress.Parse(ClientIpTextBox.Text), int.Parse(ClientPortTextBox.Text));

                try
                {
                    client.Connect(IpEnd);

                    if (client.Connected)
                    {
                        ChatScreenTextBox.AppendText("Connecté au serveur" + Environment.NewLine);
                        STW = new StreamWriter(client.GetStream());
                        STR = new StreamReader(client.GetStream());
                        STW.AutoFlush = true;
                        backgroundWorker1.RunWorkerAsync();
                        backgroundWorker2.WorkerSupportsCancellation = true;

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }
        }


        /* Boutton d'envoi du message */
        private void SendButton_Click_1(object sender, EventArgs e)
        {
            if (MessageTextBox.Text != "")
            {
                TextToSend = MessageTextBox.Text;
                backgroundWorker2.RunWorkerAsync();
            }
            MessageTextBox.Text = "";
        }


        /*Reçoit le message de l'expediteur et l'affiche*/
        private void backgroundWorker1_DoWork_1(object sender, DoWorkEventArgs e)
        {
            while (client.Connected)
            {
                try
                {
                    recieve = STR.ReadLine();
                    this.ChatScreenTextBox.Invoke(new MethodInvoker(delegate ()
                    {
                        DB db = new DB();
                        db.addMessage("Correspondant", TextToSend);
                        ChatScreenTextBox.AppendText("CORRESPONDANT : " + recieve + Environment.NewLine);
                    }));
                    recieve = "";
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }
        }

        /*Envoie le message au destinataire*/ 
        private void backgroundWorker2_DoWork_1(object sender, DoWorkEventArgs e)
        {
            if (client.Connected)
            {
                STW.WriteLine(TextToSend);
                this.ChatScreenTextBox.Invoke(new MethodInvoker(delegate ()
                {
                    DB db = new DB();
                    db.addMessage("Moi",TextToSend);
                    ChatScreenTextBox.AppendText("MOI : " + TextToSend + Environment.NewLine);
                }));
            }
            else
            {
                MessageBox.Show("Erreur d'envoi");
            }
            backgroundWorker2.CancelAsync();
        }
    }
}
