﻿namespace ChatMessenger
{
    partial class Chat
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ClientPortTextBox = new System.Windows.Forms.TextBox();
            this.ClientIpTextBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ServerPortTextBox = new System.Windows.Forms.TextBox();
            this.ServerIpTextBox = new System.Windows.Forms.TextBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.ChatScreenTextBox = new System.Windows.Forms.TextBox();
            this.MessageTextBox = new System.Windows.Forms.TextBox();
            this.SendButton = new LollipopButton();
            this.ConnectButton = new LollipopButton();
            this.lollipopLabel1 = new LollipopLabel();
            this.lollipopLabel3 = new LollipopLabel();
            this.StartButton = new LollipopButton();
            this.lollipopLabel4 = new LollipopLabel();
            this.lollipopLabel2 = new LollipopLabel();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ClientPortTextBox);
            this.groupBox2.Controls.Add(this.ClientIpTextBox);
            this.groupBox2.Controls.Add(this.ConnectButton);
            this.groupBox2.Controls.Add(this.lollipopLabel1);
            this.groupBox2.Controls.Add(this.lollipopLabel3);
            this.groupBox2.Location = new System.Drawing.Point(49, 174);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(698, 54);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "CLIENT";
            // 
            // ClientPortTextBox
            // 
            this.ClientPortTextBox.Location = new System.Drawing.Point(368, 19);
            this.ClientPortTextBox.Name = "ClientPortTextBox";
            this.ClientPortTextBox.Size = new System.Drawing.Size(166, 20);
            this.ClientPortTextBox.TabIndex = 13;
            // 
            // ClientIpTextBox
            // 
            this.ClientIpTextBox.Location = new System.Drawing.Point(92, 20);
            this.ClientIpTextBox.Name = "ClientIpTextBox";
            this.ClientIpTextBox.Size = new System.Drawing.Size(166, 20);
            this.ClientIpTextBox.TabIndex = 12;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ServerPortTextBox);
            this.groupBox1.Controls.Add(this.ServerIpTextBox);
            this.groupBox1.Controls.Add(this.StartButton);
            this.groupBox1.Controls.Add(this.lollipopLabel4);
            this.groupBox1.Controls.Add(this.lollipopLabel2);
            this.groupBox1.Location = new System.Drawing.Point(49, 98);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(698, 54);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SERVEUR";
            // 
            // ServerPortTextBox
            // 
            this.ServerPortTextBox.Location = new System.Drawing.Point(368, 20);
            this.ServerPortTextBox.Name = "ServerPortTextBox";
            this.ServerPortTextBox.Size = new System.Drawing.Size(166, 20);
            this.ServerPortTextBox.TabIndex = 12;
            // 
            // ServerIpTextBox
            // 
            this.ServerIpTextBox.Location = new System.Drawing.Point(92, 19);
            this.ServerIpTextBox.Name = "ServerIpTextBox";
            this.ServerIpTextBox.Size = new System.Drawing.Size(166, 20);
            this.ServerIpTextBox.TabIndex = 11;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork_1);
            // 
            // backgroundWorker2
            // 
            this.backgroundWorker2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker2_DoWork_1);
            // 
            // ChatScreenTextBox
            // 
            this.ChatScreenTextBox.AcceptsReturn = true;
            this.ChatScreenTextBox.AcceptsTab = true;
            this.ChatScreenTextBox.Location = new System.Drawing.Point(49, 245);
            this.ChatScreenTextBox.Multiline = true;
            this.ChatScreenTextBox.Name = "ChatScreenTextBox";
            this.ChatScreenTextBox.Size = new System.Drawing.Size(698, 135);
            this.ChatScreenTextBox.TabIndex = 18;
            // 
            // MessageTextBox
            // 
            this.MessageTextBox.Location = new System.Drawing.Point(49, 397);
            this.MessageTextBox.Name = "MessageTextBox";
            this.MessageTextBox.Size = new System.Drawing.Size(543, 20);
            this.MessageTextBox.TabIndex = 19;
            // 
            // SendButton
            // 
            this.SendButton.BackColor = System.Drawing.Color.Transparent;
            this.SendButton.BGColor = "#508ef5";
            this.SendButton.FontColor = "#ffffff";
            this.SendButton.Location = new System.Drawing.Point(623, 386);
            this.SendButton.Name = "SendButton";
            this.SendButton.Size = new System.Drawing.Size(107, 31);
            this.SendButton.TabIndex = 17;
            this.SendButton.Text = "ENVOYER";
            this.SendButton.Click += new System.EventHandler(this.SendButton_Click_1);
            // 
            // ConnectButton
            // 
            this.ConnectButton.BackColor = System.Drawing.Color.Transparent;
            this.ConnectButton.BGColor = "#508ef5";
            this.ConnectButton.FontColor = "#ffffff";
            this.ConnectButton.Location = new System.Drawing.Point(574, 17);
            this.ConnectButton.Name = "ConnectButton";
            this.ConnectButton.Size = new System.Drawing.Size(107, 31);
            this.ConnectButton.TabIndex = 11;
            this.ConnectButton.Text = "CONNEXION";
            this.ConnectButton.Click += new System.EventHandler(this.ConnectButton_Click);
            // 
            // lollipopLabel1
            // 
            this.lollipopLabel1.AutoSize = true;
            this.lollipopLabel1.BackColor = System.Drawing.Color.Transparent;
            this.lollipopLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lollipopLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))));
            this.lollipopLabel1.Location = new System.Drawing.Point(298, 19);
            this.lollipopLabel1.Name = "lollipopLabel1";
            this.lollipopLabel1.Size = new System.Drawing.Size(47, 17);
            this.lollipopLabel1.TabIndex = 8;
            this.lollipopLabel1.Text = "PORT";
            // 
            // lollipopLabel3
            // 
            this.lollipopLabel3.AutoSize = true;
            this.lollipopLabel3.BackColor = System.Drawing.Color.Transparent;
            this.lollipopLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lollipopLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))));
            this.lollipopLabel3.Location = new System.Drawing.Point(36, 20);
            this.lollipopLabel3.Name = "lollipopLabel3";
            this.lollipopLabel3.Size = new System.Drawing.Size(20, 17);
            this.lollipopLabel3.TabIndex = 2;
            this.lollipopLabel3.Text = "IP";
            // 
            // StartButton
            // 
            this.StartButton.BackColor = System.Drawing.Color.Transparent;
            this.StartButton.BGColor = "#508ef5";
            this.StartButton.FontColor = "#ffffff";
            this.StartButton.Location = new System.Drawing.Point(574, 17);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(107, 31);
            this.StartButton.TabIndex = 10;
            this.StartButton.Text = "DEMARRER";
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // lollipopLabel4
            // 
            this.lollipopLabel4.AutoSize = true;
            this.lollipopLabel4.BackColor = System.Drawing.Color.Transparent;
            this.lollipopLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lollipopLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))));
            this.lollipopLabel4.Location = new System.Drawing.Point(298, 20);
            this.lollipopLabel4.Name = "lollipopLabel4";
            this.lollipopLabel4.Size = new System.Drawing.Size(47, 17);
            this.lollipopLabel4.TabIndex = 8;
            this.lollipopLabel4.Text = "PORT";
            // 
            // lollipopLabel2
            // 
            this.lollipopLabel2.AutoSize = true;
            this.lollipopLabel2.BackColor = System.Drawing.Color.Transparent;
            this.lollipopLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lollipopLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))));
            this.lollipopLabel2.Location = new System.Drawing.Point(36, 20);
            this.lollipopLabel2.Name = "lollipopLabel2";
            this.lollipopLabel2.Size = new System.Drawing.Size(20, 17);
            this.lollipopLabel2.TabIndex = 2;
            this.lollipopLabel2.Text = "IP";
            // 
            // Chat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.MessageTextBox);
            this.Controls.Add(this.ChatScreenTextBox);
            this.Controls.Add(this.SendButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.Name = "Chat";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ChatMessenger";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LollipopButton SendButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private LollipopButton ConnectButton;
        private LollipopLabel lollipopLabel1;
        private LollipopLabel lollipopLabel3;
        private System.Windows.Forms.GroupBox groupBox1;
        private LollipopButton StartButton;
        private LollipopLabel lollipopLabel4;
        private LollipopLabel lollipopLabel2;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
        private System.Windows.Forms.TextBox ClientPortTextBox;
        private System.Windows.Forms.TextBox ClientIpTextBox;
        private System.Windows.Forms.TextBox ServerPortTextBox;
        private System.Windows.Forms.TextBox ServerIpTextBox;
        private System.Windows.Forms.TextBox ChatScreenTextBox;
        private System.Windows.Forms.TextBox MessageTextBox;
    }
}

